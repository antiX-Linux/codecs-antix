#-------------------------------------------------
#
# Project created by QtCreator 2014-02-14T11:35:17
#
#-------------------------------------------------

#/*****************************************************************************
#* codecs.pro
# *****************************************************************************
# * Copyright (C) 2014 MX Authors
# *
# * Authors: Jerry 3904
# *          Anticaptilista
# *          Adrian
# *          MX Linux <http://mxlinux.org>
# *
# * This program is free software: you can redistribute it and/or modify
# * it under the terms of the GNU General Public License as published by
# * the Free Software Foundation, either version 3 of the License, or
# * (at your option) any later version.
# *
# * MX Codecs is distributed in the hope that it will be useful,
# * but WITHOUT ANY WARRANTY; without even the implied warranty of
# * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# * GNU General Public License for more details.
# *
# * You should have received a copy of the GNU General Public License
# * along with MX Codecs.  If not, see <http://www.gnu.org/licenses/>.
# **********************************************************************/


QT       += core gui network
CONFIG   += c++11

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = codecs
TEMPLATE = app


SOURCES += main.cpp\
    lockfile.cpp \
    cmd.cpp \
    mainwindow.cpp \
    about.cpp

HEADERS  += \
    lockfile.h \
    version.h \
    cmd.h \
    mainwindow.h \
    about.h

FORMS    += \
    mainwindow.ui

TRANSLATIONS +=  translations/codecs_am.ts \ 
		translations/codecs_ar.ts \
                translations/codecs_bg.ts \
                translations/codecs_ca.ts \
                translations/codecs_cs.ts \
                translations/codecs_da.ts \
                translations/codecs_de.ts \
                translations/codecs_el.ts \
                translations/codecs_es_ES.ts \
		translations/codecs_es.ts \
                translations/codecs_fil_PH.ts \
                translations/codecs_fi.ts \
		translations/codecs_fr_BE.ts \
                translations/codecs_fr.ts \
                translations/codecs_gl_ES.ts \
                translations/codecs_he_IL.ts \
                translations/codecs_hi.ts \
                translations/codecs_hr.ts \
                translations/codecs_hu.ts \
                translations/codecs_id.ts \
                translations/codecs_it.ts \
                translations/codecs_ja.ts \
                translations/codecs_lt.ts \
                translations/codecs_mr.ts \ 
                translations/codecs_nb.ts \ 
                translations/codecs_nl.ts \
		translations/codecs_nl_BE.ts \
                translations/codecs_pl.ts \
                translations/codecs_pt.ts \
                translations/codecs_pt_BR.ts \
                translations/codecs_ro.ts \
                translations/codecs_ru.ts \
                translations/codecs_sk.ts \
                translations/codecs_sl.ts \
                translations/codecs_sq.ts \
                translations/codecs_sv.ts \
                translations/codecs_tr.ts \
                translations/codecs_uk.ts \
                translations/codecs_zh_CN.ts \
                translations/codecs_zh_HK.ts \
                translations/codecs_zh_TW.ts

RESOURCES += \
    images.qrc
