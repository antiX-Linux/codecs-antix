<?xml version="1.0" ?><!DOCTYPE TS><TS language="el" version="2.1">
<context>
    <name>MainWindow</name>
    <message>
        <location filename="mainwindow.ui" line="14"/>
        <source>Codecs Installer</source>
        <translation>Εγκατάσταση codecs</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="27"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p align=&quot;justify&quot;&gt;This application allows you to install restricted codecs that permit advanced video and audio functions.&lt;/p&gt;&lt;p align=&quot;justify&quot;&gt;In some juridictions their distribution may be limited so the user must meet local regulations. &lt;/p&gt;&lt;p align=&quot;justify&quot;&gt;&lt;br/&gt;&lt;/p&gt;&lt;p align=&quot;justify&quot;&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Do you assume legal responsibility for downloading these codecs?&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p align=&quot;justify&quot;&gt; Η εφαρμογή αυτή σας επιτρέπει να εγκαταστήσετε περιορισμένη codecs που επιτρέπουν προηγμένες λειτουργίες βίντεο και ήχου.&lt;/ p&gt;&lt;p align=&quot;justify&quot;&gt;Σε ορισμένες νομικές περιπτώσεις, η διανομή τους μπορεί να περιορίζεται για να πληρούν οι τοπικοί κανονισμοί. &lt;/ p&gt; align=&quot;justify&quot;&gt;&lt;br/&gt;&lt;/ p&gt;&lt;p align=&quot;justify&quot;&gt;&lt;span style=&quot; font-weight:600;&quot;&gt; Έχετε αναλαμβάνει τη νομική ευθύνη για τη λήψη αυτών των codecs ?&lt;/ span&gt;&lt;/ p&gt;&lt;/ body&gt;&lt;/ html&gt; </translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="44"/>
        <source>Downloading codecs files</source>
        <translation>Κατεβάζοντας τα αρχεία codecs</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="94"/>
        <source>Cancel any changes then quit</source>
        <translation>Ακύρωση τις αλλαγές και μετά κλείστε</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="97"/>
        <location filename="mainwindow.cpp" line="268"/>
        <source>Cancel</source>
        <translation>Ακύρωση</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="104"/>
        <source>Alt+N</source>
        <translation>Alt+N</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="117"/>
        <source>Apply any changes then quit</source>
        <translation>Εφαρμόστε τις αλλαγές και μετά κλείστε </translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="120"/>
        <source>OK</source>
        <translation>Εντάξει</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="175"/>
        <source>About this application</source>
        <translation>Περί της εφαρμογής</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="178"/>
        <source>About...</source>
        <translation>Περί...</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="185"/>
        <source>Alt+B</source>
        <translation>Alt+B</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="214"/>
        <source>Display help </source>
        <translation>Εμφάνιση βοήθειας</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="217"/>
        <source>Help</source>
        <translation>Βοήθεια </translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="224"/>
        <source>Alt+H</source>
        <translation>Alt+H </translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="105"/>
        <location filename="mainwindow.cpp" line="112"/>
        <location filename="mainwindow.cpp" line="120"/>
        <location filename="mainwindow.cpp" line="128"/>
        <location filename="mainwindow.cpp" line="137"/>
        <location filename="mainwindow.cpp" line="144"/>
        <location filename="mainwindow.cpp" line="153"/>
        <location filename="mainwindow.cpp" line="161"/>
        <source>&lt;b&gt;Running command...&lt;/b&gt;&lt;p&gt;</source>
        <translation>&lt;b&gt;Τρέχοντας την εντολή ...&lt;/b&gt;&lt;p&gt; </translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="108"/>
        <location filename="mainwindow.cpp" line="124"/>
        <location filename="mainwindow.cpp" line="131"/>
        <location filename="mainwindow.cpp" line="140"/>
        <location filename="mainwindow.cpp" line="146"/>
        <location filename="mainwindow.cpp" line="157"/>
        <location filename="mainwindow.cpp" line="164"/>
        <location filename="mainwindow.cpp" line="193"/>
        <location filename="mainwindow.cpp" line="246"/>
        <source>Error</source>
        <translation>Σφάλμα</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="109"/>
        <location filename="mainwindow.cpp" line="125"/>
        <location filename="mainwindow.cpp" line="141"/>
        <location filename="mainwindow.cpp" line="158"/>
        <source>Cannot connect to the download site</source>
        <translation>Δεν είναι δυνατή η σύνδεση με την τοποθεσία λήψης</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="115"/>
        <location filename="mainwindow.cpp" line="132"/>
        <location filename="mainwindow.cpp" line="147"/>
        <location filename="mainwindow.cpp" line="165"/>
        <source>Error downloading %1</source>
        <translation>Σφάλμα λήψη %1</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="170"/>
        <source>&lt;b&gt;Download Finished.&lt;/b&gt;</source>
        <translation>H lήψη oλοκληρώθηκε.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="189"/>
        <source>Installing downloaded files</source>
        <translation>Εγκατάσταση ληφθέντα αρχεία </translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="194"/>
        <source>No downloaded *.debs files found.</source>
        <translation>Δεν βρέθηκαν αρχεία *. debs</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="207"/>
        <source>&lt;b&gt;Installing...&lt;/b&gt;&lt;p&gt;</source>
        <translation>&lt;b&gt;Εγκατάσταση ...</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="220"/>
        <source>Error installing %1</source>
        <translation>Σφάλμα κατά την εγκατάσταση %1</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="224"/>
        <source>Fix missing dependencies...</source>
        <translation>Επίλυση ελλειπών εξαρτήσεων ...</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="226"/>
        <source>Error running %1 command</source>
        <translation>Σφάλμα κατά την εκτέλεση της εντολής %1</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="238"/>
        <source>&lt;b&gt;Installation process has finished&lt;/b&gt;</source>
        <translation>&lt;b&gt;H dιαδικασία εγκατάστασης έχει ολοκληρωθεί&lt;/b&gt;</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="242"/>
        <source>Finished</source>
        <translation>Ολοκλήρωση</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="243"/>
        <source>Codecs files have been downloaded and installed successfully.</source>
        <translation>Οι αρχεία codecs έχουν κατεβάσει και εγκαταστήσει με επιτυχία.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="247"/>
        <source>Process finished. Errors have occurred during the installation.</source>
        <translation>Η διαδικασία τελείωσε με λάθη. </translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="261"/>
        <source>About Codecs</source>
        <translation>Περί Codecs</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="262"/>
        <source>Codecs</source>
        <translation>Codecs</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="262"/>
        <source>Version: </source>
        <translation>Έκδοση:</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="264"/>
        <source>Simple codecs downloader for antiX Linux</source>
        <translation>Απλή λήψη codecs για antiX Linux</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="265"/>
        <source>Copyright (c) MX Linux</source>
        <translation>Πνευματικά δικαιώματα (c)  MX Linux </translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="266"/>
        <source>License</source>
        <translation>Άδεια</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="267"/>
        <location filename="mainwindow.cpp" line="277"/>
        <source>Changelog</source>
        <translation>Καταγραφή αλλαγών</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="285"/>
        <source>&amp;Close</source>
        <translation>&amp;Κλείσιμο</translation>
    </message>
</context>
<context>
    <name>QApplication</name>
    <message>
        <location filename="main.cpp" line="53"/>
        <source>Unable to get exclusive lock</source>
        <translation>Ανίκανος να πάρει αποκλειστικό κλείδωμα</translation>
    </message>
    <message>
        <location filename="main.cpp" line="54"/>
        <source>Another package management application (like Synaptic or apt-get), is already running. Please close that application first</source>
        <translation>Μια άλλη εφαρμογή διαχείρισης πακέτων (όπως Synaptic η apt-get), βρίσκεται ήδη σε λειτουργία. Κλείστε πρώτα την εφαρμογή.</translation>
    </message>
    <message>
        <location filename="main.cpp" line="67"/>
        <source>You must run this program as root.</source>
        <translation>Πρέπει να τρέξετε αυτή την εφαρμογή ως Root.</translation>
    </message>
</context>
</TS>