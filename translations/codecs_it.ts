<?xml version="1.0" ?><!DOCTYPE TS><TS language="it" version="2.1">
<context>
    <name>MainWindow</name>
    <message>
        <location filename="mainwindow.ui" line="14"/>
        <source>Codecs Installer</source>
        <translation>Installa Codecs</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="27"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p align=&quot;justify&quot;&gt;This application allows you to install restricted codecs that permit advanced video and audio functions.&lt;/p&gt;&lt;p align=&quot;justify&quot;&gt;In some juridictions their distribution may be limited so the user must meet local regulations. &lt;/p&gt;&lt;p align=&quot;justify&quot;&gt;&lt;br/&gt;&lt;/p&gt;&lt;p align=&quot;justify&quot;&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Do you assume legal responsibility for downloading these codecs?&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p align=&quot;justify&quot;&gt;Questa applicazione ti permette di installare codec proprietari che consentono funzioni audio e video avanzate.&lt;/p&gt;&lt;p align=&quot;justify&quot;&gt;In alcune giurisdizioni la loro distribuzione potrebbe avere delle restrizioni, per cui l&apos;utente deve tener conto delle regolamentazioni locali. &lt;/p&gt;&lt;p align=&quot;justify&quot;&gt;&lt;br/&gt;&lt;/p&gt;&lt;p align=&quot;justify&quot;&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Ti assumi la responsabilità legale di scaricare questi codec?&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="44"/>
        <source>Downloading codecs files</source>
        <translation>Scaricamento dei codec</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="94"/>
        <source>Cancel any changes then quit</source>
        <translation>Cancella tutte le modifiche ed esci</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="97"/>
        <location filename="mainwindow.cpp" line="268"/>
        <source>Cancel</source>
        <translation>Annulla</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="104"/>
        <source>Alt+N</source>
        <translation>Alt+N</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="117"/>
        <source>Apply any changes then quit</source>
        <translation>Applica tutte le modifiche ed esci</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="120"/>
        <source>OK</source>
        <translation>Ok</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="175"/>
        <source>About this application</source>
        <translation>Informazioni su questa applicazione</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="178"/>
        <source>About...</source>
        <translation>Info...</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="185"/>
        <source>Alt+B</source>
        <translation>Alt+B</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="214"/>
        <source>Display help </source>
        <translation>Visualizza la guida</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="217"/>
        <source>Help</source>
        <translation>Aiuto</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="224"/>
        <source>Alt+H</source>
        <translation>Alt+H</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="105"/>
        <location filename="mainwindow.cpp" line="112"/>
        <location filename="mainwindow.cpp" line="120"/>
        <location filename="mainwindow.cpp" line="128"/>
        <location filename="mainwindow.cpp" line="137"/>
        <location filename="mainwindow.cpp" line="144"/>
        <location filename="mainwindow.cpp" line="153"/>
        <location filename="mainwindow.cpp" line="161"/>
        <source>&lt;b&gt;Running command...&lt;/b&gt;&lt;p&gt;</source>
        <translation>&lt;b&gt;Comando in esecuzione...&lt;/b&gt;&lt;p&gt;</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="108"/>
        <location filename="mainwindow.cpp" line="124"/>
        <location filename="mainwindow.cpp" line="131"/>
        <location filename="mainwindow.cpp" line="140"/>
        <location filename="mainwindow.cpp" line="146"/>
        <location filename="mainwindow.cpp" line="157"/>
        <location filename="mainwindow.cpp" line="164"/>
        <location filename="mainwindow.cpp" line="193"/>
        <location filename="mainwindow.cpp" line="246"/>
        <source>Error</source>
        <translation>Errore</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="109"/>
        <location filename="mainwindow.cpp" line="125"/>
        <location filename="mainwindow.cpp" line="141"/>
        <location filename="mainwindow.cpp" line="158"/>
        <source>Cannot connect to the download site</source>
        <translation>Non posso connettermi al sito dello scaricamento</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="115"/>
        <location filename="mainwindow.cpp" line="132"/>
        <location filename="mainwindow.cpp" line="147"/>
        <location filename="mainwindow.cpp" line="165"/>
        <source>Error downloading %1</source>
        <translation>Errore di scaricamento %1</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="170"/>
        <source>&lt;b&gt;Download Finished.&lt;/b&gt;</source>
        <translation>&lt;b&gt;Scaricamento finito.&lt;/b&gt;</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="189"/>
        <source>Installing downloaded files</source>
        <translation>Installazione file scaricati</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="194"/>
        <source>No downloaded *.debs files found.</source>
        <translation>Nessun file *.debs scaricato trovato</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="207"/>
        <source>&lt;b&gt;Installing...&lt;/b&gt;&lt;p&gt;</source>
        <translation>&lt;b&gt;Installazione...&lt;/b&gt;&lt;p&gt;</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="220"/>
        <source>Error installing %1</source>
        <translation>Errore di installazione %1</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="224"/>
        <source>Fix missing dependencies...</source>
        <translation>Specificare le dipendenze mancanti</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="226"/>
        <source>Error running %1 command</source>
        <translation>Errore nell&apos;esecuzione del comando %1</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="238"/>
        <source>&lt;b&gt;Installation process has finished&lt;/b&gt;</source>
        <translation>&lt;b&gt;Il processo d&apos;installazione è terminato&lt;/b&gt;</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="242"/>
        <source>Finished</source>
        <translation>Finito</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="243"/>
        <source>Codecs files have been downloaded and installed successfully.</source>
        <translation>I codec sono stati scaricati e installati con successo.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="247"/>
        <source>Process finished. Errors have occurred during the installation.</source>
        <translation>Processo finito. Sono avvenuti degli errori durante l&apos;installazione.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="261"/>
        <source>About Codecs</source>
        <translation>Informazioni su Codecs</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="262"/>
        <source>Codecs</source>
        <translation>Codecs</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="262"/>
        <source>Version: </source>
        <translation>Versione: </translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="264"/>
        <source>Simple codecs downloader for antiX Linux</source>
        <translation>Semplice downloader di codecs per antiX Linux</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="265"/>
        <source>Copyright (c) MX Linux</source>
        <translation>Copyright (c) MX Linux</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="266"/>
        <source>License</source>
        <translation>Licenza</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="267"/>
        <location filename="mainwindow.cpp" line="277"/>
        <source>Changelog</source>
        <translation>Registro delle modifiche</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="285"/>
        <source>&amp;Close</source>
        <translation>&amp;Chiudi</translation>
    </message>
</context>
<context>
    <name>QApplication</name>
    <message>
        <location filename="main.cpp" line="53"/>
        <source>Unable to get exclusive lock</source>
        <translation>Non si riesce ad ottenere un controllo esclusivo</translation>
    </message>
    <message>
        <location filename="main.cpp" line="54"/>
        <source>Another package management application (like Synaptic or apt-get), is already running. Please close that application first</source>
        <translation>Un altro gestore di pacchetti (come Synaptic o apt-get) è già in funzione.
Prego, chiudi prima quell&apos;applicazione.</translation>
    </message>
    <message>
        <location filename="main.cpp" line="67"/>
        <source>You must run this program as root.</source>
        <translation>Devi eseguire questo programma come amministratore</translation>
    </message>
</context>
</TS>