<?xml version="1.0" ?><!DOCTYPE TS><TS language="ro" version="2.1">
<context>
    <name>MainWindow</name>
    <message>
        <location filename="mainwindow.ui" line="14"/>
        <source>Codecs Installer</source>
        <translation>Instalator Codecuri</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="27"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p align=&quot;justify&quot;&gt;This application allows you to install restricted codecs that permit advanced video and audio functions.&lt;/p&gt;&lt;p align=&quot;justify&quot;&gt;In some juridictions their distribution may be limited so the user must meet local regulations. &lt;/p&gt;&lt;p align=&quot;justify&quot;&gt;&lt;br/&gt;&lt;/p&gt;&lt;p align=&quot;justify&quot;&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Do you assume legal responsibility for downloading these codecs?&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p align=&quot;justify&quot;&gt;Aceast program poate fi folosit pentru descărcarea și instalarea de codecuri multimedia care permit utilizare unor funcții avansate video și audio.&lt;/p&gt;&lt;p align=&quot;justify&quot;&gt;În anumite jurisdicții distribuirea acestor codecuri poate fi restrictionată, utilizatorul programului trebuie să respecte legile locale. &lt;/p&gt;&lt;p align=&quot;justify&quot;&gt;&lt;br/&gt;&lt;/p&gt;&lt;p align=&quot;justify&quot;&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Vă asumați responsabilitatea pentru descărcarea acestor codecuri?&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="44"/>
        <source>Downloading codecs files</source>
        <translation>Descărcare fișiere codec</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="94"/>
        <source>Cancel any changes then quit</source>
        <translation>Anulează toate schimbările și închide programul</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="97"/>
        <location filename="mainwindow.cpp" line="268"/>
        <source>Cancel</source>
        <translation>Anulează</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="104"/>
        <source>Alt+N</source>
        <translation>Alt+N</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="117"/>
        <source>Apply any changes then quit</source>
        <translation>Aplică toate schimbările și închide programul</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="120"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="175"/>
        <source>About this application</source>
        <translation>Informații despre program</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="178"/>
        <source>About...</source>
        <translation>Despre...</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="185"/>
        <source>Alt+B</source>
        <translation>Alt+B</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="214"/>
        <source>Display help </source>
        <translation>Afișează ajutor</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="217"/>
        <source>Help</source>
        <translation>Ajutor</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="224"/>
        <source>Alt+H</source>
        <translation>Alt+H</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="105"/>
        <location filename="mainwindow.cpp" line="112"/>
        <location filename="mainwindow.cpp" line="120"/>
        <location filename="mainwindow.cpp" line="128"/>
        <location filename="mainwindow.cpp" line="137"/>
        <location filename="mainwindow.cpp" line="144"/>
        <location filename="mainwindow.cpp" line="153"/>
        <location filename="mainwindow.cpp" line="161"/>
        <source>&lt;b&gt;Running command...&lt;/b&gt;&lt;p&gt;</source>
        <translation>&lt;b&gt;Se rulează comanda...&lt;/b&gt;&lt;p&gt;</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="108"/>
        <location filename="mainwindow.cpp" line="124"/>
        <location filename="mainwindow.cpp" line="131"/>
        <location filename="mainwindow.cpp" line="140"/>
        <location filename="mainwindow.cpp" line="146"/>
        <location filename="mainwindow.cpp" line="157"/>
        <location filename="mainwindow.cpp" line="164"/>
        <location filename="mainwindow.cpp" line="193"/>
        <location filename="mainwindow.cpp" line="246"/>
        <source>Error</source>
        <translation>Eroare</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="109"/>
        <location filename="mainwindow.cpp" line="125"/>
        <location filename="mainwindow.cpp" line="141"/>
        <location filename="mainwindow.cpp" line="158"/>
        <source>Cannot connect to the download site</source>
        <translation>Nu se poate conecta la site-ul de descărcare</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="115"/>
        <location filename="mainwindow.cpp" line="132"/>
        <location filename="mainwindow.cpp" line="147"/>
        <location filename="mainwindow.cpp" line="165"/>
        <source>Error downloading %1</source>
        <translation>Eroare descărcare %1</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="170"/>
        <source>&lt;b&gt;Download Finished.&lt;/b&gt;</source>
        <translation>S-a terminat descărcarea. </translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="189"/>
        <source>Installing downloaded files</source>
        <translation>Se instalează fișierele descărcate</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="194"/>
        <source>No downloaded *.debs files found.</source>
        <translation>Nu s-a găsit nici un fisier *.deb descărcat.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="207"/>
        <source>&lt;b&gt;Installing...&lt;/b&gt;&lt;p&gt;</source>
        <translation>&lt;b&gt;Se instalează...&lt;/b&gt;&lt;p&gt;</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="220"/>
        <source>Error installing %1</source>
        <translation>Eroare instalare %1</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="224"/>
        <source>Fix missing dependencies...</source>
        <translation>Remediază dependențele lipsă...</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="226"/>
        <source>Error running %1 command</source>
        <translation>Eroare rulând comanda %1  </translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="238"/>
        <source>&lt;b&gt;Installation process has finished&lt;/b&gt;</source>
        <translation>&lt;b&gt;S-a terminat instalarea&lt;/b&gt;</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="242"/>
        <source>Finished</source>
        <translation>Sfârșit</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="243"/>
        <source>Codecs files have been downloaded and installed successfully.</source>
        <translation>Fișierele codec au fost descărcate si instalate cu succes.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="247"/>
        <source>Process finished. Errors have occurred during the installation.</source>
        <translation>Actiune terminată. Au fost detectate erori in timpul instalării. </translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="261"/>
        <source>About Codecs</source>
        <translation>Despre Codecs</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="262"/>
        <source>Codecs</source>
        <translation>Codecs</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="262"/>
        <source>Version: </source>
        <translation>Versiune:</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="264"/>
        <source>Simple codecs downloader for antiX Linux</source>
        <translation>Descărcător simplu de codecuri pentru antiX Linux</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="265"/>
        <source>Copyright (c) MX Linux</source>
        <translation>Copyright (c) MX Linux</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="266"/>
        <source>License</source>
        <translation>Licență</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="267"/>
        <location filename="mainwindow.cpp" line="277"/>
        <source>Changelog</source>
        <translation>Changelog</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="285"/>
        <source>&amp;Close</source>
        <translation>&amp;Închide</translation>
    </message>
</context>
<context>
    <name>QApplication</name>
    <message>
        <location filename="main.cpp" line="53"/>
        <source>Unable to get exclusive lock</source>
        <translation>Nu se poate obține blocare exclusivă</translation>
    </message>
    <message>
        <location filename="main.cpp" line="54"/>
        <source>Another package management application (like Synaptic or apt-get), is already running. Please close that application first</source>
        <translation>O altă aplicație de gestionare a pachetelor (cum ar fi Synaptic sau apt-get), se execută deja. Vă rugăm să închideți mai întâi această aplicație</translation>
    </message>
    <message>
        <location filename="main.cpp" line="67"/>
        <source>You must run this program as root.</source>
        <translation>Trebuie să rulați programul ca administrator.</translation>
    </message>
</context>
</TS>