<?xml version="1.0" ?><!DOCTYPE TS><TS language="id" version="2.1">
<context>
    <name>MainWindow</name>
    <message>
        <location filename="mainwindow.ui" line="14"/>
        <source>Codecs Installer</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="mainwindow.ui" line="27"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p align=&quot;justify&quot;&gt;This application allows you to install restricted codecs that permit advanced video and audio functions.&lt;/p&gt;&lt;p align=&quot;justify&quot;&gt;In some juridictions their distribution may be limited so the user must meet local regulations. &lt;/p&gt;&lt;p align=&quot;justify&quot;&gt;&lt;br/&gt;&lt;/p&gt;&lt;p align=&quot;justify&quot;&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Do you assume legal responsibility for downloading these codecs?&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p align=&quot;justify&quot;&gt;Aplikasi ini memungkinkan Anda untuk menginstall codecs terbatas yang memungkinkan fungsi video dan audio tingkat lanjut.&lt;/p&gt; &lt;p align=&quot;justify&quot;&gt;Dalam beberapa yuridiksi distribusinya mungkin terbatas sehingga pengguna harus memenuhi peraturan setempat.&lt;/p&gt;&lt;p align=&quot;justify&quot;&gt;&lt;br/&gt;&lt;/p&gt;&lt;p align=&quot;justify&quot;&gt;&lt;span style=&quot; font-weight:600;&quot;&gt; Apakah Anda bertanggung jawab secara hukum untuk mengunduh codec ini?&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="44"/>
        <source>Downloading codecs files</source>
        <translation>Unduh berkas codec</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="94"/>
        <source>Cancel any changes then quit</source>
        <translation>Batalkan perubahan lalu keluar</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="97"/>
        <location filename="mainwindow.cpp" line="268"/>
        <source>Cancel</source>
        <translation>Batal</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="104"/>
        <source>Alt+N</source>
        <translation>Alt+N</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="117"/>
        <source>Apply any changes then quit</source>
        <translation>Terapkan perubahan lalu keluar</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="120"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="175"/>
        <source>About this application</source>
        <translation>Tentang aplikasi ini</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="178"/>
        <source>About...</source>
        <translation>Tentang</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="185"/>
        <source>Alt+B</source>
        <translation>Alt+B</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="214"/>
        <source>Display help </source>
        <translation>Tampilkan bantuan</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="217"/>
        <source>Help</source>
        <translation>Bantuan</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="224"/>
        <source>Alt+H</source>
        <translation>Alt+H</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="105"/>
        <location filename="mainwindow.cpp" line="112"/>
        <location filename="mainwindow.cpp" line="120"/>
        <location filename="mainwindow.cpp" line="128"/>
        <location filename="mainwindow.cpp" line="137"/>
        <location filename="mainwindow.cpp" line="144"/>
        <location filename="mainwindow.cpp" line="153"/>
        <location filename="mainwindow.cpp" line="161"/>
        <source>&lt;b&gt;Running command...&lt;/b&gt;&lt;p&gt;</source>
        <translation>&lt;b&gt;Menjalankan perintah...&lt;/b&gt;&lt;p&gt;</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="108"/>
        <location filename="mainwindow.cpp" line="124"/>
        <location filename="mainwindow.cpp" line="131"/>
        <location filename="mainwindow.cpp" line="140"/>
        <location filename="mainwindow.cpp" line="146"/>
        <location filename="mainwindow.cpp" line="157"/>
        <location filename="mainwindow.cpp" line="164"/>
        <location filename="mainwindow.cpp" line="193"/>
        <location filename="mainwindow.cpp" line="246"/>
        <source>Error</source>
        <translation>Galat</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="109"/>
        <location filename="mainwindow.cpp" line="125"/>
        <location filename="mainwindow.cpp" line="141"/>
        <location filename="mainwindow.cpp" line="158"/>
        <source>Cannot connect to the download site</source>
        <translation>Tidak dapat terhubung dengan situs unduhan</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="115"/>
        <location filename="mainwindow.cpp" line="132"/>
        <location filename="mainwindow.cpp" line="147"/>
        <location filename="mainwindow.cpp" line="165"/>
        <source>Error downloading %1</source>
        <translation>Galat mengunduh %1</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="170"/>
        <source>&lt;b&gt;Download Finished.&lt;/b&gt;</source>
        <translation>&lt;b&gt;Unduhan selesai.&lt;/b&gt;</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="189"/>
        <source>Installing downloaded files</source>
        <translation>Menginstall file-file terunduh</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="194"/>
        <source>No downloaded *.debs files found.</source>
        <translation>Tidak ada file *.debs yang terunduh</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="207"/>
        <source>&lt;b&gt;Installing...&lt;/b&gt;&lt;p&gt;</source>
        <translation>&lt;b&gt;Menginstall...&lt;/b&gt;&lt;p&gt;</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="220"/>
        <source>Error installing %1</source>
        <translation>Galat menginstal %1</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="224"/>
        <source>Fix missing dependencies...</source>
        <translation>Memperbaiki dependensi yang hilang...</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="226"/>
        <source>Error running %1 command</source>
        <translation>Kesalahan menjalankan perintah %1
 

 
 
 
 
 </translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="238"/>
        <source>&lt;b&gt;Installation process has finished&lt;/b&gt;</source>
        <translation>&lt;b&gt;Proses instalasi telah selesai&lt;/b&gt;</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="242"/>
        <source>Finished</source>
        <translation>Selesai</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="243"/>
        <source>Codecs files have been downloaded and installed successfully.</source>
        <translation>File Codec telah diunduh dan instalasi berhasil</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="247"/>
        <source>Process finished. Errors have occurred during the installation.</source>
        <translation>Proses selesai. Galat telah terjadi selama proses instalasi</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="261"/>
        <source>About Codecs</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="262"/>
        <source>Codecs</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="262"/>
        <source>Version: </source>
        <translation>Versi:</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="264"/>
        <source>Simple codecs downloader for antiX Linux</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="265"/>
        <source>Copyright (c) MX Linux</source>
        <translation>Copyright (c) MX Linux</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="266"/>
        <source>License</source>
        <translation>Lisensi</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="267"/>
        <location filename="mainwindow.cpp" line="277"/>
        <source>Changelog</source>
        <translation>Changelog</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="285"/>
        <source>&amp;Close</source>
        <translation>&amp;Tutup</translation>
    </message>
</context>
<context>
    <name>QApplication</name>
    <message>
        <location filename="main.cpp" line="53"/>
        <source>Unable to get exclusive lock</source>
        <translation>Tidak dapat exclusive lock</translation>
    </message>
    <message>
        <location filename="main.cpp" line="54"/>
        <source>Another package management application (like Synaptic or apt-get), is already running. Please close that application first</source>
        <translation>Ada aplikasi manajemen paket lain ( seperti Synaptic atau apt-get ), yang sedang berjalan. Mohon tutup dulu aplikasi tersebut</translation>
    </message>
    <message>
        <location filename="main.cpp" line="67"/>
        <source>You must run this program as root.</source>
        <translation>Anda harus menjalankan program ini sebagai root</translation>
    </message>
</context>
</TS>